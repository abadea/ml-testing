import torch
import torch.nn as nn
import torch.nn.functional as f
from torch.utils.data import DataLoader, TensorDataset
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor

import h5py
import numpy as np
from sklearn.model_selection import train_test_split
import argparse
import os
import datetime

from trainTransformer import ABPhiC_to_ABCosSinPhiC, PtEtaPhiE_to_PtEtaPhiM
from Set2Set import Set2Set
from Transformer import Embed 

from train import DeltaR
import itertools


#%%%%%%% Helper Functions %%%%%%%%#

def covariant_update(mlp, w, mx):
    mw = mlp(mx)
    # update eta
    eta = (w[:,:,0] + mw[:,:,0]).unsqueeze(-1)
    # extract rotation cos(theta), sin(theta)
    u = mw[:,:,[1,2]] + torch.cat([torch.ones(mw.shape[0],mw.shape[1],1), torch.zeros(mw.shape[0],mw.shape[1],1)], -1).to(mw.device)
    u = torch.nn.functional.normalize(u, p=2, dim=-1)
    ucos, usin = u.split([1,1],-1)
    # make rotation matrix and rotate
    u = torch.stack([torch.cat([ucos, -1*usin],-1),torch.cat([usin, ucos],-1)],-1)
    u = torch.einsum("nmij, nmj -> nmi", u, w[:,:,[1,2]])
    # concat to be updated
    w = torch.cat([eta,u],-1)
    return w

def pairwise(x):
    ''' input x = (eta, cos(phi), sin(phi)), output matrix xij = (eta_i - eta_j, cos(phi_i - phi_j), sin(phi_i - phi_j)) '''
    eta, cphi, sphi = x.split((1, 1, 1), dim=-1) 
    eta_ij = cast(eta, eta, "subtract")
    sin_ij = cast(sphi, cphi, "times") - cast(cphi, sphi, "times")
    cos_ij = cast(cphi, cphi, "times") + cast(sphi, sphi, "times")
    xij = torch.stack([eta_ij, cos_ij, sin_ij], -1)
    return xij

def cast(a, b, op="add"):
    ''' a and b are batched (NEvents, NJets, NObs) '''
    a = a.repeat(1, 1, a.shape[1])
    b = b.repeat(1, 1, b.shape[1]).transpose(2,1)
    if op == "add":
        return a + b
    elif op == "subtract":
        return a - b
    elif op == "times":
        return a*b
    return a+b

def DeltaRMinPermutation(p, y, N):
    ''' input eta, cos(phi), sin(phi) batched '''
    
    i = torch.Tensor(list(itertools.permutations(range(N)))).long().to(p.device)

    p_etaphi = torch.stack([p[:,:,0], torch.atan2(p[:,:,2],p[:,:,1])],-1) # sin/cos
    y_etaphi = torch.stack([y[:,:,0], torch.atan2(y[:,:,2],y[:,:,1])],-1)

    p_etaphi = p_etaphi[:,:,i]
    y_etaphi = y_etaphi.unsqueeze(2).repeat(1,1,i.shape[0],1)


    # get indices
    dR = DeltaR(p_etaphi[:,0], y_etaphi[:,0], p_etaphi[:,1], y_etaphi[:,1])
    dR = dR.mean(-1)
    dR = torch.argmin(dR,-1)

    # get permutation
    i = i[dR]

    return i

def loss_abs_p(p, wp, y, wy):

    # print(p.shape, wp.shape, y.shape, wy.shape)

    # perform delta r minimization to permute
    idx = DeltaRMinPermutation(wp,wy, 2)
    p = torch.gather(p,1,idx.unsqueeze(-1).repeat(1,1,p.shape[-1]).long())
    wp = torch.gather(wp,1,idx.unsqueeze(-1).repeat(1,1,wp.shape[-1]).long())

    # compute loss
    loss_invariant = torch.nn.functional.l1_loss(p[:,:,[0,1]], y, reduction="none") # just focus on pt, m for now
    loss_covariant = torch.nn.functional.l1_loss(wp, wy, reduction="none")
    loss = torch.cat([loss_invariant, loss_covariant],-1).sum(-1).mean(-1).mean(-1)
    #loss = loss_covariant.sum(-1).mean(-1).mean(-1)
    # # px = pt*cos(phi), py = pt*sin(phi), eta, mass
    # loss = torch.nn.functional.l1_loss(
    #     torch.stack([p[:,:,0] * wp[:,:,1], p[:,:,0] * wp[:,:,2], wp[:,:,0], p[:,:,1]],-1),
    #     torch.stack([y[:,:,0] * wy[:,:,1], y[:,:,0] * wy[:,:,2], wy[:,:,0], y[:,:,1]],-1),
    #     reduction="none")
    # # sum and average over gluinos
    # loss = loss.sum(-1).mean(-1).mean(-1)

    return loss

#%%%%%%% Classes %%%%%%%%#
class AttnBlock(nn.Module):

    def __init__(self,
                 embed_dim = 4,
                 num_heads = 1,
                 attn_dropout = 0,
                 add_bias_kv = False,
                 kdim = None,
                 vdim = None,
                 ffwd_on = False,
                 # ffwd_dim = 16,
                 ffwd_dims = [16,16],
                 ffwd_dropout = 0):
        super().__init__()

        self.attn = nn.MultiheadAttention(embed_dim, num_heads, dropout=attn_dropout, add_bias_kv=add_bias_kv, batch_first=True, kdim=kdim, vdim=vdim)
        self.post_attn_norm = nn.LayerNorm(embed_dim)
        self.ffwd_on = ffwd_on
        if ffwd_on:
            self.ffwd, input_dim = [], embed_dim
            for dim in ffwd_dims:
                self.ffwd.extend([
                    nn.Linear(input_dim, dim),
                    nn.LayerNorm(dim),
                    nn.ReLU(),
                ])
                input_dim = dim
            self.ffwd.append(nn.Linear(input_dim, embed_dim)) # bring back to embed_dim
            self.ffwd = nn.Sequential(*self.ffwd)
            self.post_ffwd_norm = nn.LayerNorm(embed_dim)
        

    def forward(self, query, key, value, key_padding_mask=None, attn_mask=None, flip_query_value=False, return_attn_values=False, add_init_residual=True):

        residual = value

        if flip_query_value:
            query, value = value, query
        
        # attention
        value, weights = self.attn(query, key, value, key_padding_mask=key_padding_mask, attn_mask = attn_mask, need_weights=return_attn_values)

        # skip connection and norm
        temp = value
        if add_init_residual:
            value += residual
        value = self.post_attn_norm(value)

        # feed forward and skip connection
        if self.ffwd_on:
            residual = value
            value = self.ffwd(value)
            value += residual
            value = self.post_ffwd_norm(value)

        if return_attn_values:
            return value, weights #temp
            
        return value

class CovariantAttentionEncoder(nn.Module):

    def __init__(self, embed_input_dim, embed_dims, mlp_input_dim, mlp_dims, attn_block_cfgs, covariant_mlp_input_dim, covariant_mlp_dims):

        super().__init__()

        # embed
        self.embed = Embed(embed_input_dim, embed_dims, normalize_input=True)

        # mlp
        self.mlp = Embed(mlp_input_dim, mlp_dims, normalize_input=False, final_layer=[mlp_dims[-1],1])

        # attention blocks
        self.blocks = nn.ModuleList([AttnBlock(**cfg) for cfg in attn_block_cfgs])

        # update covariant variables
        self.covariant_mlp = Embed(covariant_mlp_input_dim, covariant_mlp_dims, normalize_input=False, final_layer=[covariant_mlp_dims[-1], covariant_mlp_dims[-1]]) if len(covariant_mlp_dims) else None

    def forward(self, x, w, mask):

        # embed and remask
        x = self.embed(torch.cat([x,w],-1))
        x = x.masked_fill(mask.unsqueeze(-1).repeat(1,1,x.shape[-1]), 0)

        # attention mechanism
        for ib, block in enumerate(self.blocks):

            # pairwise + MLP
            if ib == 0 or self.covariant_mlp:
                wij = pairwise(w)
                wij = self.mlp(wij)
                wij = wij.squeeze(-1)

            # apply attention and remask
            x, mx = block(query=x, key=x, value=x, key_padding_mask=mask, attn_mask=wij, return_attn_values=True)
            x = x.masked_fill(mask.unsqueeze(-1).repeat(1,1,x.shape[-1]), 0)

            # update covariant variables
            if self.covariant_mlp:
                w = covariant_update(self.covariant_mlp, w, mx)

        # remask
        x = x.masked_fill(mask.unsqueeze(-1).repeat(1,1,x.shape[-1]), 0)
        w = w.masked_fill(mask.unsqueeze(-1).repeat(1,1,w.shape[-1]), 0)

        return x, w

class CovariantAttentionDecoder(nn.Module):

    def __init__(self, input_dim_set2set, num_step_set2set, num_layer_set2set, num_target_objs, mlp_input_dim, mlp_dims, self_attn_block_cfgs, cross_attn_block_cfgs, covariant_mlp_input_dim, covariant_mlp_dims):
        
        super().__init__()

        # set to set for invariant feature vector initialization
        # self.Set2Set = nn.ModuleList([Set2Set(input_dim=input_dim_set2set, n_iters=num_step_set2set, n_layers=num_layer_set2set) for i in range(num_target_objs)])
        self.Set2Set = Set2Set(input_dim=input_dim_set2set, n_iters=num_step_set2set, n_layers=num_layer_set2set, act_fn=nn.ReLU)

        # mlp for covariant objects
        self.mlp = Embed(mlp_input_dim, mlp_dims, normalize_input=False, final_layer=[mlp_dims[-1],1])

        # self attention blocks
        self.self_blocks = nn.ModuleList([AttnBlock(**cfg) for cfg in self_attn_block_cfgs])

        # cross attention blocks
        self.cross_blocks = nn.ModuleList([AttnBlock(**cfg) for cfg in cross_attn_block_cfgs])

        # update covariant variables
        self.covariant_mlp = Embed(covariant_mlp_input_dim, covariant_mlp_dims, normalize_input=False, final_layer=[covariant_mlp_dims[-1], covariant_mlp_dims[-1]]) if len(covariant_mlp_dims) else None


    def forward(self, x, w, mask, y=None, wy=None):

        # use Set2Set to get initial candidates
        # x_out = []
        # for s2s in self.Set2Set:
        #     x_out.append(s2s(x))
        # x_out = torch.stack(x_out,1)
        
        x_out = self.Set2Set(x)
        x_out = x_out.reshape(-1,2,x.shape[-1])


        # store loss 
        loss = 0

        # perform attention
        for ib, (selfb, xb) in enumerate(zip(self.self_blocks, self.cross_blocks)):

            # x_out, mx = xb(query=x, key=x, value=x_out, flip_query_value=True, return_attn_values=True) # x attention
            x_out, mx = xb(query=x_out, key=x, value=x, key_padding_mask=mask, flip_query_value=False, return_attn_values=True, add_init_residual=False)
            # print(f"cross: {x_out[0]}, {mx[0]}")

            # initialize w_out
            if ib == 0:
                w_out = torch.matmul(mx,w)
                w_out[:,:,[1,2]] = torch.nn.functional.normalize(w_out[:,:,[1,2]],2,-1)

            # update covariant variables
            if self.covariant_mlp:
                w_out = covariant_update(self.covariant_mlp, w_out, mx)

            # compute pairwise
            wij_out = pairwise(w_out)
            wij_out = self.mlp(wij_out)
            wij_out = wij_out.squeeze(-1)

            # compute self attention
            x_out, mx = selfb(query=x_out, key=x_out, value=x_out, attn_mask=wij_out, return_attn_values=True)
            # print(f"self: {x_out[0]}, {mx[0]}")

            # compute loss if target provided for all but the last layer
            if y is not None and ib+1 < len(self.self_blocks):
                loss += loss_abs_p(x_out, w_out, y, wy)

        # average loss over blocks
        if len(self.self_blocks) > 1:
            loss /= (len(self.self_blocks)-1)

        return x_out, w_out, loss

class StepLightning(pl.LightningModule):

    def __init__(self,
                 encoder_config = {},
                 decoder_config = {},
                 lr = 1e-3,
                 init_weights = False):
        super().__init__()

        self.encoder = CovariantAttentionEncoder(**encoder_config)
        self.decoder = CovariantAttentionDecoder(**decoder_config)
        self.lr = lr

        if init_weights:
            self.apply(self._init_weights)

    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            torch.nn.init.xavier_uniform_(module.weight)
            torch.nn.init.zeros_(module.bias)
        if isinstance(module, Set2Set):
           torch.nn.init.xavier_uniform_(module.pred.weight)
           torch.nn.init.zeros_(module.pred.bias)
        if isinstance(module, nn.ModuleList):
            for m in module:
                if isinstance(m, Set2Set):
                    torch.nn.init.xavier_uniform_(m.pred.weight)
                    torch.nn.init.zeros_(m.pred.bias)

    def forward(self, x, w, mask, y=None, wy=None):

        x, w = self.encoder(x, w, mask)
        x, w, loss = self.decoder(x, w, mask, y, wy)
        return x, w, loss

    def train_valid_step(self, batch, batch_idx):

        x, w, mask, y, wy  = batch
        # scale
        y = y/100

        # predict
        x, w, loss_inter = self.forward(x, w, mask, y, wy)
        
        # compute loss
        loss_final = loss_abs_p(x, w, y, wy)
        
        loss = loss_final # + loss_inter
        return loss

    def training_step(self, batch, batch_idx):
        loss = self.train_valid_step(batch, batch_idx)
        cur_lr = self.trainer.optimizers[0].param_groups[0]['lr']
        self.log("lr", cur_lr, prog_bar=True, on_step=True)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.train_valid_step(batch, batch_idx)
        self.log("val_loss", loss, prog_bar=True, on_step=True)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.parameters(), lr=self.lr)
        return optimizer

def loadData(inFile, N=-1, test_size=-1, batch_size=256, shuffle=True):

    #~~~ Load Data ~~~#
    with h5py.File(inFile, "r") as hf:
        x = np.stack([np.array(hf[f'source/{key}']) for key in ["pt","eta","phi","mass"]],1)
        y = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],1)
    
    N = x.shape[0] if N == -1 else N

    # convert to tensor
    x, y = torch.Tensor(x), torch.Tensor(y)
    x = ABPhiC_to_ABCosSinPhiC(x)
    y = ABPhiC_to_ABCosSinPhiC(PtEtaPhiE_to_PtEtaPhiM(y))

    # remove events with nan's even though they should not exist TO-DO: understand why these exist in CreateH5files
    mask = x.sum(1).sum(1)
    mask = mask==mask
    x, y = x[mask], y[mask]

    # flip axis 
    x = x.transpose(2,1)
    y = y.transpose(2,1)
    # network expects a mask value of 1 where pad is
    mask = (x[:,:,0] == 0)

    # normalize
    temp = ~mask # only use non masked values
    # mass and pt log normalize
    # x[:,:,[0,4]] = torch.log(x[:,:,[0,4]]).clip(1e-5) # it can happen where there is a zero mass for some reason so clip it for now
    # get mean, std of non-padded entries
    mean, std = x[temp].mean(0), x[temp].std(0)
    print(mean,std)

    # standardize
    # x[:,:,[0,4]] -= mean[[0,4]]
    # x[:,:,[0,4]] /= std[[0,4]]
    print(x[temp].mean(0), x[temp].std(0))

    # remask
    x = x.masked_fill(mask.unsqueeze(-1).repeat(1,1,x.shape[-1]), 0)
    
    # extract leading N
    x, y, mask = x[:N], y[:N], mask[:N]

    # get indices for w and x
    widx = [1,2,3]
    xidx = [i for i in range(x.shape[-1]) if i not in widx]

    # split data
    if test_size != -1:
        x_train, x_test, mask_train, mask_test, y_train, y_test = train_test_split(x, mask, y, test_size=test_size, shuffle=shuffle)
        print(f"x_train {x_train.shape}, y_train {y_train.shape}, x_test {x_test.shape}, y_test {y_test.shape}")
        # split into x, w
        w_train, w_test, wy_train, wy_test = x_train[:,:,widx], x_test[:,:,widx], y_train[:,:,[1,2,3]], y_test[:,:,[1,2,3]]
        x_train, x_test, y_train, y_test = x_train[:,:,xidx], x_test[:,:,xidx], y_train[:,:,[0,4]], y_test[:,:,[0,4]]
        # make loaders
        train_dataloader = DataLoader(TensorDataset(x_train, w_train, mask_train, y_train, wy_train), batch_size=batch_size, shuffle=shuffle, num_workers=0)
        val_dataloader = DataLoader(TensorDataset(x_test, w_test, mask_test, y_test, wy_test), batch_size=batch_size, shuffle=shuffle, num_workers=0)
        return train_dataloader, val_dataloader

    w = x[:,:,widx]
    x = x[:,:,xidx]
    wy = y[:,:,[1,2,3]]
    y = y[:,:,[0,4]]

    return x, w, y, wy, mask

def main():

    # user options
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--inFile", help="Input training file.", default=None, required=True)
    parser.add_argument("-s", "--switch", help="Switch between signal models (GG_rpv, GG_rpv_viaN1).", default="GG_rpv")
    parser.add_argument("-o", "--outDir", help="File name of the output directory", default="./")
    parser.add_argument("-e", "--epochs", help="Number of epochs to train on", default=1, type=int)
    parser.add_argument("-b", "--batch_size", help="Batch size", default=2, type=int)
    parser.add_argument("-d", "--device", help="Device to use.", default=None)
    parser.add_argument("-lr", "--learning_rate", help="Learning rate", default=1e-3, type=float)
    parser.add_argument("-w", "--weights", help="Pretrained weights to use as starting point", default=None)
    parser.add_argument("-p", "--predict", help="Just do prediction", action="store_true")
    parser.add_argument("-n", "--nevents", help="Number of events to train on. Default -1 equals all events.", default=-1, type=int)
    ops = parser.parse_args()

    # decide on device
    device = ops.device 
    if not device:
        device = "gpu" if torch.cuda.is_available() else "cpu"

    NInvariant, NCovariants = 50, 3 #2, 3
    FFWD = 100
    E = 50
    num_encoder_blocks = 6
    encoder_config = {
        "embed_input_dim":5,
        "embed_dims":[E,E,E],
        "covariant_mlp_input_dim":E, 
        "mlp_input_dim":NCovariants, "mlp_dims":[FFWD, FFWD], 
        "attn_block_cfgs":[{"embed_dim":E, "ffwd_on":True, "ffwd_dims" : [FFWD,FFWD]}]*num_encoder_blocks, # , "ffwd_dim":64
        "covariant_mlp_dims":[]
    }
    num_decoder_blocks = 6
    decoder_config = {
        "input_dim_set2set" : E, "num_step_set2set":6, "num_layer_set2set":3, "num_target_objs":2,
        "mlp_input_dim" : NCovariants, "mlp_dims" : [FFWD, FFWD],
        "self_attn_block_cfgs" : [{"embed_dim" : E, "ffwd_on": True, "ffwd_dims" : [FFWD, FFWD]}]*num_decoder_blocks,
        "cross_attn_block_cfgs" : [{"embed_dim" : E, "ffwd_on": True, "ffwd_dims" : [FFWD, FFWD]}]*num_decoder_blocks,
        "covariant_mlp_input_dim" : 20, # number of input jets
        "covariant_mlp_dims":[FFWD, FFWD, FFWD, NCovariants]
    }
    model = StepLightning(encoder_config, decoder_config, ops.learning_rate, True)
    if ops.weights:
        ckpt = torch.load(ops.weights,map_location="cpu")
        model.load_state_dict(ckpt["state_dict"])
    
    # predict
    if ops.predict:

        x, w, y, wy, mask = loadData(ops.inFile, ops.nevents)

        # quick prediction
        model.eval()
        with torch.no_grad():
            p, wp, loss = model(x, w, mask)
            p, wp = p.detach().numpy(), wp.detach().numpy()
       
        print(p.mean((0,1)), wp.mean((0,1)))
        print(p.std((0,1)), wp.std((0,1)))

        outData = {"y":y,"wy": wy,"p":100*p,"wp":wp}
        with h5py.File(os.path.join(ops.outDir,"testCov.h5"),"w") as hf:
            for key, val in outData.items():
                hf.create_dataset(key,data=val)
        return
        
    shuffle=True
    train_dataloader, val_dataloader = loadData(ops.inFile, ops.nevents, 0.25, ops.batch_size, shuffle)
    print(len(train_dataloader), len(val_dataloader))

    # make checkpoint dir
    checkpoint_dir = os.path.join(ops.outDir, f'checkpoints/training_{datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S")}')
    if not os.path.isdir(checkpoint_dir):
        os.makedirs(checkpoint_dir)

    # callbacks
    callbacks = [
        ModelCheckpoint(monitor="val_loss", dirpath=checkpoint_dir, filename='cp-{epoch:04d}', save_top_k=1), # 0=no models, -1=all models, N=n models, set save_top_k=-1 to save all checkpoints
        LearningRateMonitor(logging_interval='epoch')
    ]

    # torch lightning trainer
    trainer = pl.Trainer(
        # overfit_batches=0.1,
        # detect_anomaly=True,
        # gradient_clip_val=0.5,
        # track_grad_norm=2,
        accelerator=device,
        devices=1,
        max_epochs=ops.epochs,
        log_every_n_steps=5,
        callbacks=callbacks,
        precision=32,
        auto_lr_find=False,
        default_root_dir=checkpoint_dir
        # auto_scale_batch_size="binsearch"
    )
    
    # fit
    trainer.fit(model, train_dataloader, val_dataloader)
    
    # save model
    trainer.save_checkpoint(os.path.join(checkpoint_dir,"finalWeights.ckpt"))

if __name__ == "__main__":
    main()
