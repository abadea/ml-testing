
# standard
import h5py
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
import os
import gc
import datetime
import itertools

# torch
import torch
from torch.utils.data import DataLoader, TensorDataset
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelSummary, EarlyStopping, ModelCheckpoint, LearningRateMonitor
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau, CyclicLR

# custom
from ParticleTransformer import Step, PtEtaPhiM_to_PxPyEtaM, PtEtaPhiE_to_PxPyEtaM, EPxPyPz_to_PtEtaPhiE, EPxPyPz_to_PtEtaPhiM, PtEtaPhiM_to_EPxPyPz, PtEtaPhiE_to_EPxPyPz, EPxPyPz_to_PtEtaCosSinPhiM, EPxPyPz_to_PtM, EPxPyPz_to_Pt, EPxPyPz_to_Pt2

def DeltaPhi(phi1,phi2):
    ''' delta phi = phi_1 - phi_2 needs to be handled carefully. copying the implementation from root into numpy vectorization 
    https://root.cern.ch/doc/v606/GenVector_2VectorUtil_8h_source.html#l00061 '''
    dphi = phi1 - phi2
    dphi[dphi > torch.pi] -= 2.0*torch.pi
    dphi[dphi <= -torch.pi] += 2.0*torch.pi
    return dphi

def DeltaEta(eta1,eta2):
	deta = eta1 - eta2
	return deta

def DeltaR(eta1, eta2, phi1, phi2):
	dphi = DeltaPhi(phi1,phi2)
	deta = DeltaEta(eta1,eta2)
	dr = torch.sqrt(deta**2 + dphi**2)
	return dr

class StepNetwork(pl.LightningModule):
	def __init__(self, config, y_shapes, lr=1e-3):
		super().__init__()

		# make layers
		self.steps = []
		for step, conf in config.items():
			self.steps.append(Step(**conf))
		self.steps = torch.nn.ModuleList(self.steps)

		# other params
		self.lr = lr
		self.y_shapes = y_shapes
		print(y_shapes)
		self.y_perms = [torch.Tensor(list(itertools.permutations(range(N))), device=self.device).long() for N in [(j-i) for i,j in y_shapes]]
		print(self.y_perms)
		print(self.device)
		
		# running means for loss
		# self.mu_px = 0
		# self.mu_py = 0
		# # contribution scaling to loss
		# self.l_mu_px = 1
		# self.l_mu_py = 1
		
		self.save_hyperparameters()

	def forward(self, x, v=None, mask=None):
		mask = (x[:,0]!=0).unsqueeze(1).bool() # check for pt 0 (N,1,P)
		#print(torch.isfinite(x))
		#print(torch.isnan(x))
		for step in self.steps:
			x, v, mask, fraction = step(x, v=v, mask=mask)
		return x, fraction

	def train_valid_step(self, batch, batch_idx):
		# x, v, mask, y = batch
	    
		x, y = batch
		v = None # particle interactions if we want them
		mask = (x[:,0]!=0).unsqueeze(1).bool() # check for pt 0 (N,1,P)

		# split the y's
		ys = [y[:,:,i:j] for (i,j) in self.y_shapes]

		loss = 0
		for iStep, (step, yi) in enumerate(zip(self.steps, ys)):
			#print(f"Input: {x.shape}, {x[0]}")
			#print(x[0])
			x, v, mask, fraction = step(x, v=v, mask=mask)
			loss += self.loss_PxPyEtaM(x,yi,fraction,iStep)

		return loss

	def training_step(self, batch, batch_idx):
		loss = self.train_valid_step(batch, batch_idx)
		cur_lr = self.trainer.optimizers[0].param_groups[0]['lr']
		self.log("lr", cur_lr, prog_bar=True, on_step=True)
		self.log("train_loss", loss)
		return loss

	def validation_step(self, batch, batch_idx):
		loss = self.train_valid_step(batch, batch_idx)
		self.log("val_loss", loss)
		return loss

	def configure_optimizers(self):
		optimizer = torch.optim.AdamW(self.parameters(), lr=self.lr)
		# # lr_schedulers = {"scheduler": CyclicLR(optimizer,self.lr,1e-3,cycle_momentum=False), #ReduceLROnPlateau(optimizer, patience=30, factor=0.1),
		# # 		 "monitor": "val_loss",
		# # 		 'name': 'lr_scheduler'}
		return optimizer # [optimizer], [lr_schedulers]

	def loss_PxPyEtaM(self, inp, target, fractions, iStep):
		''' target is (px,py,eta,m) and inp is (pt,eta,phi,m) then do L1 norm '''
		#inp = PtEtaPhiM_to_PxPyEtaM(inp) # could speed up by doing EPxPyPz_to_PxPyEtaM within step
		
		# compute all possible delta R
		# i = self.y_perms[iStep].to(self.device)

		# # extract etaphi
		# inp_etaphi = inp[:,[1,2]] # fix because inp is eta,cos(phi),sin(phi)
		# #inp_etaphi[:,1] = torch.arccos(inp_etaphi[:,1])
		# target_etaphi = target[:,[1,2]]
		# #target_etaphi[:,1] = torch.arccos(target_etaphi[:,1])
			
		# # make permutations
		# inp_etaphi = inp_etaphi[:,:,i]
		# target_etaphi = target.unsqueeze(2).repeat(1,1,i.shape[0],1)

		# # compute dR, average, get argmin
		# dR = DeltaR(inp_etaphi[:,0], target_etaphi[:,0], inp_etaphi[:,1], target_etaphi[:,1]) # need to update this
		# dR = dR.mean(-1)
		# dR = torch.argmin(dR,-1)

		# # get permutation from argmin
		# i = i[dR]
		
		# # use permutation to permute input
		# inp = torch.gather(inp,-1,i.unsqueeze(1).repeat(1,inp.shape[1],1).long())

		# loss function
		#loss = torch.nn.functional.mse_loss(inp[:,[0,1,3]], target[:,[0,1,3]], reduction='mean') # mse_loss
		# loss = torch.nn.functional.mse_loss(torch.log(inp[:,0]+10), torch.log(target[:,0]+10), reduction='mean') # pt
		# loss += torch.nn.functional.mse_loss(inp[:,1], target[:,1], reduction='mean') # eta
		# loss += (2*(1-torch.cos(inp[:,2] - target[:,2]))).mean() # phi
		# loss += torch.nn.functional.mse_loss(torch.log(inp[:,3]), torch.log(target[:,3]), reduction='mean') # mass
		#loss += (1*torch.exp(-0.5*( (fractions[:,:,:2]-0.5)/0.12)**2)).mean()
		#loss = loss.mean()

                # loss of e,px,py,pz
		#loss = torch.nn.functional.l1_loss(inp, target, reduction='mean') # [:,[1,2]]
		#loss += (100*torch.exp(-0.5*( (fractions[:,:,:2]-0.5)/0.125)**2)).mean()
		loss = torch.nn.functional.mse_loss(torch.log(inp), torch.log(target), reduction="mean")

		#loss = torch.nn.functional.l1_loss(inp, target, reduction='mean')
		return loss

if __name__ == "__main__":

	# user options
	parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-i", "--inFile", help="Input training file.", default=None, required=True)
	parser.add_argument("-s", "--switch", help="Switch between signal models (GG_rpv, GG_rpv_viaN1).", default="GG_rpv")
	parser.add_argument("-o", "--outDir", help="File name of the output directory", default="./")
	parser.add_argument("-e", "--epochs", help="Number of epochs to train on", default=1, type=int)
	parser.add_argument("-b", "--batch_size", help="Batch size", default=2, type=int)
	parser.add_argument("-d", "--device", help="Device to use.", default=None)
	parser.add_argument("-lr", "--learning_rate", help="Learning rate", default=1e-3, type=float)
	parser.add_argument("-w", "--weights", help="Pretrained weights to use as starting point", default=None)
	ops = parser.parse_args()

	# decide on device
	device = ops.device 
	if not device:
		device = "gpu" if torch.cuda.is_available() else "cpu"

	# global switch
	switch = ops.switch

	# configurationsÏ
	confs = {}

	final_embed_dim1 = 3 # 2 gluino, 1 other (ISR, pileup, etc.) --> Question would it be good to first go NJets -> 6 quarks then 6 quarks + ISR/Pileup/Etc -> 2 gluino's
	# --> To-do: can I actually constrain the ISR/Pileup/ETC with the truth monte carlo? If so increase this to 7
	confs["GG_rpv"] = {
		"file" : ops.inFile,
		"conf" : {
			"step1" : {
				"embed_dims" :[16, 32, 16], #[64, 256, 64, final_embed_dim1], # 128, 512, 128
				"num_heads" : 4, #final_embed_dim1,
				"remove_last_particle" : True,
				"fc_params" : [(16,0)],
				"num_classes" : final_embed_dim1,
				"input_dim" : 5,
				"num_layers" : 3,
				"activation" : "gelu"
			}
		},
		"y_shapes" : [(0,2)]
	}

	final_embed_dim1 = 7 # 2 neutralino, 4 quarks, 1 other (ISR, pileup, etc.) --> To-do: can I actually constrain the ISR/Pileup/ETC with the truth monte carlo? If so increase this to 7
	final_embed_dim2 = 2 # 2 gluino
	confs["GG_rpv_viaN1"] = {
		"file" : ops.inFile, 
		"conf" : {
			"step1" : {
				"embed_dims" :[32, 64, 32],
				"num_heads" : 8,
				"remove_last_particle" : True,
				"fc_params" : [(32,0), (32,0)],
				"num_classes" : final_embed_dim1,
				"input_dim" : 4
			},
			"step2" : {
				"embed_dims" :[32, 64, 32],
				"num_heads" : 8,
				"remove_last_particle" : False,
				"fc_params" : [(32,0), (32,0)],
				"num_classes" : final_embed_dim2,
				"input_dim" : 4
			}
		},
		"y_shapes" : [(0,6), (6,8)]
	}

	# create network
	model = StepNetwork(config=confs[switch]["conf"], lr=ops.learning_rate, y_shapes=confs[switch]["y_shapes"])
	if ops.weights:
	    ckpt = torch.load(ops.weights,map_location="cpu")
	    model.load_state_dict(ckpt["state_dict"])
	
	#~~~ Load Data ~~~#
	with h5py.File(confs[switch]["file"], "r") as hf:
		x = np.stack([np.array(hf[f'source/{key}']) for key in ["pt","eta","phi","mass"]],1) # [N, C=4mom, P] desired input shape
		
		# get targets as (pt,eta,phi,e)
		if switch == "GG_rpv":
			y1 = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],1) # TO-DO: update this to be mass also
			y = [y1]
		elif switch == "GG_rpv_viaN1":
			neutralino = np.stack([np.array(hf[f'truth/truth_NeutralinoFromGluino_{key}']) for key in ["pt","eta","phi","e"]],1)
			quarks = np.stack([np.array(hf[f'truth/truth_QuarkFromGluino_{key}']) for key in ["pt","eta","phi","e"]],1)
			y1 = np.concatenate([neutralino,quarks],-1)
			y2 = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],1)
			y = [y1,y2]
			
		# save original shapes and concat
		y_shapes = [i.shape[-1] for i in y]
		y = np.concatenate(y,-1)
	
	# convert to tensor
	N = 500 #111355
	x, y = torch.Tensor(x)[:N], torch.Tensor(y)[:N]
	
	# sort y phi ordered
	#idx = np.expand_dims(np.argsort(y[:,2],-1),1).repeat(4,1)
	#y = np.take_along_axis(y,idx,2)
        
	# only keep low pt gluino events
	# idx = (y[:,0] > 1000).sum(1).bool()
	# print(x.shape, y.shape)
	# x = x[idx]
	# y = y[idx]
	# print(x.shape, y.shape)

	x = EPxPyPz_to_PtEtaCosSinPhiM(PtEtaPhiM_to_EPxPyPz(x), nanToNum=True)
	y = EPxPyPz_to_Pt2(PtEtaPhiE_to_EPxPyPz(y), nanToNum=True)

	# x = PtEtaPhiM_to_EPxPyPz(x)
	# y = PtEtaPhiE_to_EPxPyPz(y)

	# remove events with nan's even though they should not exist TO-DO: understand why these exist in CreateH5files
	mask = x.sum(1).sum(1)
	mask = mask==mask
	x = x[mask]
	y = y[mask]
	print(f"Number of events with nan: {(~mask).sum()}")
	print(np.where(x!=x), np.where(y!=y))

	# split data
	x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, shuffle=True)
	print(f"x_train {x_train.shape}, y_train {y_train.shape}, x_test {x_test.shape}, y_test {y_test.shape}")
	train_dataloader = DataLoader(TensorDataset(x_train, y_train), batch_size=ops.batch_size, shuffle=True, num_workers=0)
	val_dataloader = DataLoader(TensorDataset(x_test, y_test), batch_size=ops.batch_size, shuffle=False, num_workers=0)

	# cleanup
	del x, y, x_train, x_test, y_train, y_test
	gc.collect()

	# make checkpoint dir
	checkpoint_dir = os.path.join(ops.outDir, f'checkpoints/training_{datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S")}')
	if not os.path.isdir(checkpoint_dir):
		os.makedirs(checkpoint_dir)

	# callbacks
	callbacks = [
		# ModelSummary(max_depth=-1),
		#EarlyStopping(monitor="val_loss", mode="min", min_delta=0.0, patience=10, verbose=True),
		ModelCheckpoint(monitor="val_loss", dirpath=checkpoint_dir, filename='cp-{epoch:04d}', save_top_k=5), # 0=no models, -1=all models, N=n models
	    LearningRateMonitor(logging_interval='epoch')
	]

	# torch lightning trainer
	trainer = pl.Trainer(
		#overfit_batches=0.1,
		#detect_anomaly=True,
                #gradient_clip_val=0.5,
                #track_grad_norm=2,
		accelerator=device,
		devices=1,
		max_epochs=ops.epochs,
		log_every_n_steps=5,
		callbacks=callbacks,
		precision=32,
		auto_lr_find=True,
		default_root_dir=checkpoint_dir
		#auto_scale_batch_size="binsearch"
	)
	
	# tune for learning rate then fit
	# trainer.tune(model, train_dataloader, val_dataloader)
	trainer.fit(model, train_dataloader, val_dataloader)
	
	# save model
	trainer.save_checkpoint(os.path.join(checkpoint_dir,"finalWeights.ckpt"))

