import h5py
import numpy as np
import matplotlib.pyplot as plt
from ParticleTransformer import EPxPyPz_to_PtEtaPhiM, PtEtaPhiM_to_EPxPyPz, PtEtaCosSinPhiM_to_EPxPyPz
import torch
import argparse

# user options
parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-i", "--inFile", help="Input training file.", default=None, required=True)
parser.add_argument("-s", "--switch", help="Switch between signal models (GG_rpv, GG_rpv_viaN1).", default="GG_rpv")
parser.add_argument("-o", "--outDir", help="File name of the output directory", default="./")
ops = parser.parse_args()

x = h5py.File(ops.inFile)
y = np.array(x['y'])
yh = np.array(x['y_hat'])
print(f"Input shapes: {y.shape}, {yh.shape}")
#w = np.array(x['normweight']) * 139000 # scale by lumi 139fb-1 = 139,000 pb-1
w = np.ones(y.shape[0])
we = np.expand_dims(w,-1).repeat(2,1).flatten()
print(f"Weight shapes: {w.shape}, {we.shape}")

# if e,px,py,pz prediction
# y1 = EPxPyPz_to_PtEtaPhiM(torch.Tensor(y)).numpy()
# yh1 = EPxPyPz_to_PtEtaPhiM(torch.Tensor(yh)).numpy()
# y = np.concatenate([y1,y],1)
# yh = np.concatenate([yh1,yh],1)

# if pt,eta,phi,m prediction
y1 = PtEtaPhiM_to_EPxPyPz(torch.Tensor(y)).numpy()
yh1 = PtEtaPhiM_to_EPxPyPz(torch.Tensor(yh)).numpy()
y = np.concatenate([y,y1],1)
yh = np.concatenate([yh,yh1],1)

# # if pt,eta,cosphi,sinphi,m prediction
# y0 = np.stack([y[:,0], y[:,1], np.arctan2(y[:,3],y[:,2]), y[:,4]], 1) # pt, eta, tan^-1 (sin(phi)/cos(phi)), m
# yh0 = np.stack([yh[:,0], yh[:,1], np.arctan2(yh[:,3],yh[:,2]), yh[:,4]], 1)
# y1 = PtEtaCosSinPhiM_to_EPxPyPz(torch.Tensor(y)).numpy()
# yh1 = PtEtaCosSinPhiM_to_EPxPyPz(torch.Tensor(yh)).numpy()
# y = np.concatenate([y0,y1],1)
# yh = np.concatenate([yh0,yh1],1)

# if pt prediction but pt,eta,cosphi,sinphi,m truth
# y0 = np.stack([y[:,0], y[:,1], np.arctan2(y[:,3],y[:,2]), y[:,4]], 1)
# yh0 = np.stack([np.sqrt(yh[:,0]), np.ones(yh[:,0].shape), np.ones(yh[:,0].shape), np.ones(yh[:,0].shape)], 1) # pt, eta, tan^-1 (sin(phi)/cos(phi)), m
# y1 = PtEtaCosSinPhiM_to_EPxPyPz(torch.Tensor(y)).numpy()
# yh1 = PtEtaPhiM_to_EPxPyPz(torch.Tensor(yh0)).numpy()
# y = np.concatenate([y0,y1],1)
# yh = np.concatenate([yh0,yh1],1)

print(f"Input shapes after concatenate: {y.shape}, {yh.shape}")
print(f"Target mean: {np.mean(y,0)}")
print(f"Target std: {np.std(y,0)}")
print(f"Pred mean: {np.mean(yh,0)}")
print(f"Pred std: {np.std(yh,0)}")

#plot
bins = [np.linspace(0,3000,100),np.linspace(-5,5,50),np.linspace(-np.pi,np.pi,50),np.linspace(1800,2200,20), np.linspace(1500,4000,100), np.linspace(-2500,2500,100), np.linspace(-2500,2500,100), np.linspace(-3500,3500,100)]
labels = ["pt","eta","phi","m","e","px","py","pz"]

nRow = 7
nCol = 4
scale = 3
fig, axs = plt.subplots(nRow, nCol, figsize=(nRow*scale,nCol*scale))
for i, ax in enumerate(axs.flat):
    if y.shape[0] == yh.shape[0]:
        ax.hist(y[:,i].flatten(),bins=bins[i],histtype="step",color="blue",label="target",weights=we);
    ax.hist(yh[:,i].flatten(),bins=bins[i],histtype="step",color="red",label="pred", weights=we);
    ax.set(xlabel=labels[i],ylabel="counts");
    ax.legend()
    print(f"{labels[i]}: {yh[:,i][:2]}")
    if i+1 == y.shape[1]:
        break
    
for j in [0,1,2,3]: #[4,5,6,7]: #range(3,y.shape[1]):
    i+=1
    l = labels[j]
    b = bins[j]
    yht = yh[:,j]
    axs.flat[i].hist2d(yht[:,0],yht[:,1], bins=(b, b), cmap=plt.cm.jet, weights=w)
    axs.flat[i].set(xlabel=f"Predicted gluino 0 {l}",ylabel=f"Predicted gluino 1 {l}")

    if y.shape[0] == yh.shape[0]:
        i+=1
        yt = y[:,j]
        axs.flat[i].hist2d(yt[:,0],yt[:,1], bins=(b, b), cmap=plt.cm.jet, weights=w)
        axs.flat[i].set(xlabel=f"Truth gluino 0 {l}",ylabel=f"Truth gluino 1 {l}")
        
        i+=1
        axs.flat[i].hist2d(yht[:,0],yt[:,0], bins=(b, b), cmap=plt.cm.jet, weights=w)
        axs.flat[i].set(xlabel=f"Predicted gluino 0 {l}",ylabel=f"Truth gluino 0 {l}")
        
        i+=1
        axs.flat[i].hist2d(yht[:,1],yt[:,1], bins=(b, b), cmap=plt.cm.jet, weights=w)
        axs.flat[i].set(xlabel=f"Predicted gluino 1 {l}",ylabel=f"Truth gluino 1 {l}")

    
# i+=1
# bins=np.linspace(0,5000,100)
# if y.shape[0] == yh.shape[0]:
#     axs.flat[i].hist((0.5*(y[:,3][:,0] + y[:,3][:,1])).flatten(),bins=bins,histtype="step",color="blue",label="target", weights=w);
# axs.flat[i].hist((0.5*(yh[:,3][:,0] + yh[:,3][:,1])).flatten(),bins=bins,histtype="step",color="red",label="pred", weights=w);
# axs.flat[i].set(xlabel="average mass",ylabel="counts");
# axs.flat[i].legend()

fig.tight_layout()
plt.savefig("plot.pdf",bbox_inches='tight')
#plt.show()

#plt.show()
