# pytorch
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelSummary, EarlyStopping, ModelCheckpoint, LearningRateMonitor

# dgl
import dgl

# standard
import h5py
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
import os
import gc
import datetime
import itertools

# custom
from ParticleTransformer import PtEtaPhiE_to_EPxPyPz, EPxPyPz_to_PtEtaPhiM
from Set2Set import Set2Set

class Embed(nn.Module):
    def __init__(self, input_dim, dims, normalize_input=True, final_layer=None):
        super().__init__()

        self.input_bn = nn.BatchNorm1d(input_dim) if normalize_input else None
        module_list = []
        for dim in dims:
            module_list.extend([
                nn.Linear(input_dim, dim),
                nn.LayerNorm(dim),
                nn.ReLU(),
            ])
            input_dim = dim
        if final_layer is not None:
            module_list.append(nn.Linear(final_layer[0], final_layer[1]))
        self.embed = nn.Sequential(*module_list)

    def forward(self, x):
        if self.input_bn is not None:
            x = self.input_bn(x.transpose(1,2)) # must transpose because batchnorm expects N,C,L rather than N,L,C like multihead
            x = x.transpose(1,2) # revert transpose
        x = self.embed(x)
        return x

class AttnBlock(nn.Module):

    def __init__(self,
                 embed_dim = 4,
                 num_heads = 1,
                 attn_dropout = 0,
                 add_bias_kv = False,
                 kdim = None,
                 vdim = None,
                 ffwd_on = False,
                 ffwd_dim = 16,
                 ffwd_dropout = 0):
        super().__init__()

        self.attn = nn.MultiheadAttention(embed_dim, num_heads, dropout=attn_dropout, add_bias_kv=add_bias_kv, batch_first=True, kdim=kdim, vdim=vdim)
        self.post_attn_norm = nn.LayerNorm(embed_dim)
        self.ffwd_on = ffwd_on
        if ffwd_on:
            self.ffwd = nn.Sequential(
                nn.Linear(embed_dim, ffwd_dim),
                nn.LayerNorm(ffwd_dim),
                nn.ReLU(), 
                nn.Dropout(ffwd_dropout),
                nn.Linear(ffwd_dim, embed_dim)
            )
            self.post_ffwd_norm = nn.LayerNorm(embed_dim)

    def forward(self, query, key, value, key_padding_mask=None, attn_mask=None, flip_query_value=False, return_attn_values=False):

        residual = value

        if flip_query_value:
            query, value = value, query
        
        value, _ = self.attn(query, key, value, key_padding_mask=key_padding_mask, attn_mask = attn_mask, need_weights=False)
        temp = value
        value += residual
        value = self.post_attn_norm(value)
        residual = value
        if self.ffwd_on:
            value = self.ffwd(value)
            value += residual
            value = self.post_ffwd_norm(value)

        if return_attn_values:
            return value, temp
            
        return value

class Encoder(nn.Module):

    def __init__(self,
                 input_dim = 4,
                 embed_input = False,
                 embed_dims = [16],
                 attn_block_cfgs = []):
        super().__init__()

        # embedding
        self.embed_input = embed_input
        if embed_input:
            self.embed = Embed(input_dim, embed_dims, normalize_input=True)

        # attention blocks
        self.blocks = nn.ModuleList([AttnBlock(**cfg) for cfg in attn_block_cfgs])

    def forward(self, x, key_padding_mask=None, attn_mask=None):

        # perform embedding
        if self.embed_input:
            x = self.embed(x)

        # attention mechanism
        for block in self.blocks:
            x = block(query=x, key=x, value=x, key_padding_mask=key_padding_mask, attn_mask=attn_mask)

        return x

class Decoder(nn.Module):

    def __init__(self,
                 self_attn_block_cfgs = [],
                 cross_attn_block_cfgs = [],
                 nTargetJets = 2,
                 ffwd_on = False,
                 ffwd_dim = 4,
                 ffwd_dropout = 0,
                 num_target_objs = 2,
                 num_step_set2set = 6,
                 num_layer_set2set = 3):
        super().__init__()

        # attention blocks
        self.self_blocks = nn.ModuleList([AttnBlock(**cfg) for cfg in self_attn_block_cfgs])
        self.cross_blocks = nn.ModuleList([AttnBlock(**cfg) for cfg in cross_attn_block_cfgs])
        
        # final linear layer
        self.ffwd_on = ffwd_on
        if ffwd_on:
            embed_dim = cross_attn_block_cfgs[-1]["embed_dim"] # change dimensionality to target ffwd_dim
            self.ffwd = nn.Sequential(
                nn.Linear(embed_dim, ffwd_dim),
                nn.LayerNorm(ffwd_dim),
                nn.ReLU(), 
                nn.Dropout(ffwd_dropout),
                nn.Linear(ffwd_dim, ffwd_dim)
            )

        # for candidate creation
        self.nTargetJets = nTargetJets
        self.self_embed_dim = self_attn_block_cfgs[-1]["embed_dim"]
        self.Set2Set = nn.ModuleList([Set2Set(input_dim=self.self_embed_dim, n_iters=num_step_set2set, n_layers=num_layer_set2set) for i in range(num_target_objs)])
        

    def forward(self, x, target=None):

        # use Set2Set to get initial candidates
        y = []
        for s2s in self.Set2Set:
            # print(s2s.pred.weight[0])
            y.append(s2s(x))
        y = torch.stack(y,1)

        # perform attention
        loss = 0
        for selfb, xb in zip(self.self_blocks, self.cross_blocks):
            y = selfb(query=y, key=y, value=y) #, key_padding_mask=value_key_padding_mask, attn_mask=value_attn_mask) # self attention
            y = xb(query=x, key=x, value=y, flip_query_value=True) # x attention
            # sum up intermediate loss function
            if target is not None:
                loss += torch.nn.functional.l1_loss(y[:,:,[0,1]], target, reduction="mean")

        # average over decoding blocks
        loss /= len(self.cross_blocks)

        if self.ffwd_on:
            y = self.ffwd(y)

        return y, loss

class Step(nn.Module):

    def __init__(self,
                 encoder_config = {},
                 decoder_config = {}):
        super().__init__()

        self.encoder = Encoder(**encoder_config)
        self.decoder = Decoder(**decoder_config)

    def forward(self, x, mask):

        x = self.encoder(x, key_padding_mask=mask, attn_mask=None)
        print(f"After Encode: {x.shape}")
        x = self.decoder(x)

        return x

class StepLightning(pl.LightningModule):

    def __init__(self,
                 encoder_config = {},
                 decoder_config = {},
                 lr = 1e-3,
                 init_weights = False):
        super().__init__()

        self.encoder = Encoder(**encoder_config)
        self.decoder = Decoder(**decoder_config)
        self.lr = lr

        if init_weights:
            self.apply(self._init_weights)

    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            torch.nn.init.xavier_uniform_(module.weight)
            torch.nn.init.zeros_(module.bias)
        if isinstance(module, Set2Set):
            torch.nn.init.xavier_uniform_(module.pred.weight)
            torch.nn.init.zeros_(module.pred.bias)
        if isinstance(module, nn.ModuleList):
            for m in module:
                if isinstance(m, Set2Set):
                    torch.nn.init.xavier_uniform_(m.pred.weight)
                    torch.nn.init.zeros_(m.pred.bias)

    def forward(self, x):

        x = self.encoder(x, key_padding_mask=None, attn_mask=None)
        x, loss_intermediate = self.decoder(x)
        return x

    def train_valid_step(self, batch, batch_idx):

        x, y = batch

        x = self.encoder(x)
        x, loss_intermediate = self.decoder(x, y[:,:,[0,3]])

        # compute loss
        loss_final = torch.nn.functional.l1_loss(x[:,:,[0,1]], y[:,:,[0,3]], reduction="mean") # just focus on pt, m for now
        loss = loss_final #+ loss_intermediate
        return loss

    def training_step(self, batch, batch_idx):
        loss = self.train_valid_step(batch, batch_idx)
        cur_lr = self.trainer.optimizers[0].param_groups[0]['lr']
        self.log("lr", cur_lr, prog_bar=True, on_step=True)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.train_valid_step(batch, batch_idx)
        self.log("val_loss", loss)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.parameters(), lr=self.lr)
        # lr_schedulers = {"scheduler": CyclicLR(optimizer,self.lr,1e-3,cycle_momentum=False), #ReduceLROnPlateau(optimizer, patience=30, factor=0.1),
        #          "monitor": "val_loss",
        #          'name': 'lr_scheduler'}
        return [optimizer] #, [lr_schedulers]


def test_AttnBlock():

    for ffwd_on in [False, True]:
        m = AttnBlock(ffwd_on=ffwd_on)
        nEvents, nJets, EmbedDim = 3, 7, 4
        x = torch.ones((nEvents, nJets, EmbedDim))
        y = m(x, x, x)
        print(f"ffwd_on ({ffwd_on}): {x.shape}, {y.shape}")

def test_Encoder():

    input_dim = 4
    embed_dims = [16]
    for embed_input in [False, True]:
        attn_block_cfgs = [{"embed_dim" : embed_dims[-1] if embed_input else input_dim, "ffwd_on" : True}]
        m = Encoder(input_dim=input_dim, embed_input=embed_input, embed_dims=embed_dims, attn_block_cfgs=attn_block_cfgs)
        nEvents, nJets, InputDim = 3, 7, 4
        x = torch.ones((nEvents, nJets, InputDim))
        y = m(x)
        print(f"embed_input ({embed_input}): {x.shape}, {y.shape}")

def test_Decoder():

    cross_attn_block_cfgs = [{"embed_dim" : 4, "ffwd_on": True}]
    self_attn_block_cfgs = [{"embed_dim" : 4, "ffwd_on": True}]

    m = Decoder(self_attn_block_cfgs=self_attn_block_cfgs, cross_attn_block_cfgs=cross_attn_block_cfgs)
    
    # output of encoder
    nEvents, nJets, EmbedDim = 3, 7, 4
    cross = torch.ones((nEvents, nJets, EmbedDim))

    # candidate 4mom
    nEvents, nTargetJets, EmbedDim = 3, 2, 4
    value = torch.ones((nEvents, nJets, EmbedDim)) # NOTE: need to set value to have nJet dimension rather than nTargetJets because of matrix multiplications. I think the mask should remove unused values from gradients
    value_key_padding_mask = torch.cat([torch.zeros((nEvents,nTargetJets)), torch.ones((nEvents,nJets-nTargetJets))],dim=1) # (N,S=nJets) mask the excess jets

    y = m(cross, value, value_key_padding_mask=value_key_padding_mask, value_attn_mask=None)
    print(f"{cross.shape}, {value.shape}, {y.shape}")

def test_EncoderDecoder():

    # encoder
    input_dim = 4
    embed_dims = [16,8] # NOTE: does not work if embed_dims[-1] != decoder input EmbedDim (4mom of target gluino)
    attn_block_cfgs = [{"embed_dim" : embed_dims[-1], "ffwd_on" : True}]
    encoder = Encoder(input_dim=input_dim, embed_input=True, embed_dims=embed_dims, attn_block_cfgs=attn_block_cfgs)

    # decoder
    cross_attn_block_cfgs = [{"embed_dim" : embed_dims[-1], "ffwd_on": True}]
    self_attn_block_cfgs = [{"embed_dim" : embed_dims[-1], "ffwd_on": True}]
    nTargetJets = 2
    decoder = Decoder(self_attn_block_cfgs=self_attn_block_cfgs, cross_attn_block_cfgs=cross_attn_block_cfgs, nTargetJets=nTargetJets)

    # input
    nEvents, nJets, InputDim = 3, 7, 4
    x = torch.ones((nEvents, nJets, InputDim))

    # candidate 4mom for decoder
    # nEvents, nTargetJets, EmbedDim = 3, 2, embed_dims[-1]
    # value = torch.ones((nEvents, nJets, EmbedDim)) # NOTE: need to set value to have nJet dimension rather than nTargetJets because of matrix multiplications. I think the mask should remove unused values from gradients
    # value_key_padding_mask = torch.cat([torch.zeros((nEvents,nTargetJets)), torch.ones((nEvents,nJets-nTargetJets))],dim=1) # (N,S=nJets) mask the excess jets

    x = encoder(x)
    print(x.shape)
    x = decoder(cross=x) #, value=value, value_key_padding_mask=value_key_padding_mask, value_attn_mask=None)
    print(x.shape)

def test_Step():

    encoder_config = {
        "input_dim" : 4,
        "embed_input" : True,
        "embed_dims" : [16]
    }
    encoder_config["attn_block_cfgs"] = [{"embed_dim" : encoder_config["embed_dims"][-1], "ffwd_on" : True}] * 6

    decoder_config = {
        "nTargetJets" : 2,
        "cross_attn_block_cfgs" : [{"embed_dim" : encoder_config["embed_dims"][-1], "ffwd_on": True, "kdim" : encoder_config["embed_dims"][-1], "vdim" : encoder_config["embed_dims"][-1]}] * 6,
        "self_attn_block_cfgs" : [{"embed_dim" : encoder_config["embed_dims"][-1], "ffwd_on": True}] * 6, #encoder_config["embed_dims"][-1]
        "ffwd_on" : True,
        "ffwd_dim" : 4,
        "num_target_objs" : 2
    }

    m = Step(encoder_config=encoder_config, decoder_config=decoder_config)

    # input
    nEvents, nJets, InputDim = 3, 20, 4
    x = torch.ones((nEvents, nJets, InputDim))
    nNotPad = 2
    mask = torch.cat([torch.zeros((nEvents,nNotPad)), torch.ones((nEvents,nJets-nNotPad))],dim=1) # mask where there is a 1

    print(f"Input {x.shape}")
    y = m(x,mask)
    print(f"Output {y.shape}")

if __name__ == "__main__":
    # print("Testing AttnBlock")
    # test_AttnBlock()
    # print("Testing Encoder")
    # test_Encoder()
    # print("Testing Decoder")
    # test_Decoder()
    # print("Testing Encoder then Decoder")
    # test_EncoderDecoder()
    # print("Testing Step")
    test_Step()


