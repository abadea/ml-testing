# standard
import h5py
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
import os
import gc
import datetime
#import matplotlib.pyplot as plt

# torch
import torch
from torch.utils.data import DataLoader, TensorDataset
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelSummary, EarlyStopping, ModelCheckpoint

# custom
from ParticleTransformer import PtEtaPhiM_to_EPxPyPz, PtEtaPhiE_to_EPxPyPz, EPxPyPz_to_PtEtaPhiM, EPxPyPz_to_PtEtaCosSinPhiM, EPxPyPz_to_PtM
from train import StepNetwork

if __name__ == "__main__":

    # user options
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--inFile", help="Input training file.", default=None, required=True)
    parser.add_argument("-s", "--switch", help="Switch between signal models (GG_rpv, GG_rpv_viaN1).", default="GG_rpv")
    parser.add_argument("-o", "--outDir", help="File name of the output directory", default="./")
    parser.add_argument("-w", "--weights", help="Pretrained weights to use as starting point", default=None)
    ops = parser.parse_args()

    # global switch
    switch = ops.switch

    # configurations
    confs = {}

    final_embed_dim1 = 3 # 2 gluino, 1 other (ISR, pileup, etc.) --> Question would it be good to first go NJets -> 6 quarks then 6 quarks + ISR/Pileup/Etc -> 2 gluino's
    # --> To-do: can I actually constrain the ISR/Pileup/ETC with the truth monte carlo? If so increase this to 7
    confs["GG_rpv"] = {
        "file" : ops.inFile,
        "conf" : {
            "step1" : {
                "embed_dims" :[16,32,16], #[64, 256, 64, final_embed_dim1], # 128, 512, 128
                "num_heads" : 4, #final_embed_dim1,
                "remove_last_particle" : True,
                "fc_params" : [(16,0)],
                "num_classes" : final_embed_dim1,
                "input_dim" : 5,
                "num_layers" : 3,
                "activation" : "gelu"
            }
	},
	"y_shapes" : [(0,2)]
    }

    final_embed_dim1 = 7 # 2 neutralino, 4 quarks, 1 other (ISR, pileup, etc.) --> To-do: can I actually constrain the ISR/Pileup/ETC with the truth monte carlo? If so increase this to 7
    final_embed_dim2 = 2 # 2 gluino
    confs["GG_rpv_viaN1"] = {
        "file" : ops.inFile, 
        "conf" : {
            "step1" : {
                "embed_dims" :[32, 64, 32],
                "num_heads" : 8,
                "remove_last_particle" : True,
                "fc_params" : [(32,0), (32,0)],
                "num_classes" : final_embed_dim1,
                "input_dim" : 4
            },
            "step2" : {
                "embed_dims" :[32, 64, 32],
                "num_heads" : 8,
                "remove_last_particle" : False,
                "fc_params" : [(32,0), (32,0)],
                "num_classes" : final_embed_dim2,
                "input_dim" : 4
            }
	},
	"y_shapes" : [(0,6), (6,8)]
    }

    # create network
    model = StepNetwork(config=confs[switch]["conf"], y_shapes=confs[switch]["y_shapes"])
    ckpt = torch.load(ops.weights,map_location="cpu")
    model.load_state_dict(ckpt["state_dict"])

    #~~~ Load Data ~~~#
    with h5py.File(confs[switch]["file"], "r") as hf:
        x = np.stack([np.array(hf[f'source/{key}']) for key in ["pt","eta","phi","mass"]],1) # [N, C=4mom, P] desired input shape
        w = np.array(hf[f'EventVars/normweight'])
        print(x.shape, w.shape)
        if "Dijet" in confs[switch]["file"]:
            y = torch.ones((1,4,2))
            #idx = np.random.choice(x.shape[0],int(1e6))
            #x = x[idx]
            #w = w[idx]
        else:
            if switch == "GG_rpv":
                y1 = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],1) # TO-DO: update this to be mass also
                y = [y1]
            elif switch == "GG_rpv_viaN1":
                neutralino = np.stack([np.array(hf[f'truth/truth_NeutralinoFromGluino_{key}']) for key in ["pt","eta","phi","e"]],1)
                quarks = np.stack([np.array(hf[f'truth/truth_QuarkFromGluino_{key}']) for key in ["pt","eta","phi","e"]],1)
                y1 = np.concatenate([neutralino,quarks],-1)
                y2 = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],1)
                y = [y1,y2]
                # save original shapes and concat
            y = np.concatenate(y,-1)
        
    # convert to tensor
    N = -1 #500 #20000
    x, y = torch.Tensor(x)[:N], torch.Tensor(y)[:N]
    w = w[:N]
    # idx = (y[:,0] < 500).sum(1).bool()
    # print(idx.shape, x.shape, y.shape)
    # x = x[idx]
    # y = y[idx]
    # w = w[idx]
    # print(x.shape, y.shape)

    x = EPxPyPz_to_PtEtaCosSinPhiM(PtEtaPhiM_to_EPxPyPz(x), nanToNum=True)
    y = EPxPyPz_to_PtEtaCosSinPhiM(PtEtaPhiE_to_EPxPyPz(y), nanToNum=True)
    
    # x = PtEtaPhiM_to_EPxPyPz(x)
    # y = PtEtaPhiE_to_EPxPyPz(y)

    # remove events with nan's even though they should not exist TO-DO: understand why these exist in CreateH5files
    mask = x.sum(1).sum(1)
    mask = mask==mask
    x = x[mask]
    w = w[mask]
    if "Dijet" not in confs[switch]["file"]:
        y = y[mask]
    print(f"Number of events with nan: {(~mask).sum()}")

    # predict
    print(f"Input shape: {x.shape}")
    
    model.eval()
    with torch.no_grad():
        y_hat = []
        fractions = []
        batch_size = int(10000)
        for i in range(int(np.ceil(x.shape[0]/batch_size))):
            s = int(i*batch_size)
            e = s + batch_size
            print(s,e)
            y_hat_temp, fractions_temp = model(x[s:e])
            y_hat.append(y_hat_temp)
            fractions.append(fractions_temp)
        y_hat = torch.concat(y_hat)
        fractions = torch.concat(fractions)
        print(y_hat.shape)
            
    outData = {"y":y,"y_hat":y_hat,"normweight":w,"fractions":fractions}
    with h5py.File(os.path.join(ops.outDir,"test.h5"),"w") as hf:
        for key, val in outData.items():
            hf.create_dataset(key,data=val)
