import h5py
import numpy as np
import matplotlib.pyplot as plt

x=h5py.File("testCov.h5","r")

N = -1
if N == -1:
	N = x['y'].shape[0]
y, wy = np.array(x['y'])[:N], np.array(x['wy'])[:N]
p, wp = np.array(x['p'])[:N], np.array(x['wp'])[:N]

# extract pt and mass
idx = [0,1]
p, y = p[:,:,idx], y[:,:,idx]

yphi = np.expand_dims(np.arctan2(wy[:,:,2],wy[:,:,1]),-1)
pphi = np.expand_dims(np.arctan2(wp[:,:,2],wp[:,:,1]),-1)

y = np.concatenate([y,wy,yphi],-1)
p = np.concatenate([p,wp,pphi],-1)
print(y.shape, p.shape)

#plot
bins = [np.linspace(0,3000,50),np.linspace(0,2500,50),np.linspace(-5,5,20),np.linspace(-1,1,20), np.linspace(-1,1,20), np.linspace(-np.pi,np.pi,20)]
labels = [r"p$_{\mathrm{T}}$","m",r"$\eta$",r"cos($\phi$)",r"sin($\phi$)",r"$\phi$"]

nRow, nCol = 4, 4 # number
sRow, sCol = 24, 14 # fig scaling
fig, axs = plt.subplots(nRow, nCol, figsize=(sRow, sCol))
for i, ax in enumerate(axs.flat):
    ax.hist(y[:,:,i].flatten(),bins=bins[i],histtype="step",color="blue",label="target")
    ax.hist(p[:,:,i].flatten(),bins=bins[i],histtype="step",color="red",label="pred")
    ax.set(xlabel=labels[i],ylabel="counts");
    ax.legend()
    # ax.set_yscale("log")
    if i+1 == y.shape[-1]:
        break

for j in [0,2,5]:
	i+=1
	ax = axs.flat[i]
	ax.hist( (p[:,:,j].flatten() - y[:,:,j].flatten())/y[:,:,j].flatten(), bins=np.linspace(-10,10,50), histtype="step", color="blue")
	ax.set(xlabel=fr"$\Delta$ {labels[j]}/{labels[j]}", ylabel="counts")

for j in [0,1,2,3,4,5]:
    i+=1
    ax = axs.flat[i]
    dy = np.abs(y[:,:,j][:,0] - y[:,:,j][:,1]).flatten() # / (y[:,:,j][:,0] + y[:,:,j][:,1]).flatten()
    dp = np.abs(p[:,:,j][:,0] - p[:,:,j][:,1]).flatten() # / (p[:,:,j][:,0] + p[:,:,j][:,1]).flatten()
    ax.hist(dy, bins=np.linspace(0,10,50), histtype="step", color="blue", label="target")
    ax.hist(dp, bins=np.linspace(0,10,50), histtype="step", color="red", label="pred")
    ax.set(xlabel=fr"$\Delta$/sum {labels[j]} between gluinos", ylabel="counts")
    ax.legend()
#plt.savefig("/eos/home-a/abadea/analysis/rpvmj/plots/matching/plotConv.pdf",bbox_inches='tight')
plt.show()
