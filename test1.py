# standard
import h5py
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
import os
import gc
import datetime
import itertools

# torch
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset

# pytorch lightning
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor

class Embed(nn.Module):
    def __init__(self, input_dim, dims, normalize_input=True):
        super().__init__()

        self.input_bn = nn.BatchNorm1d(input_dim) if normalize_input else None
        module_list = []
        for dim in dims:
            module_list.extend([
                nn.Linear(input_dim, dim),
                nn.LayerNorm(dim),
                nn.ReLU(),
            ])
            input_dim = dim
        self.embed = nn.Sequential(*module_list)

    def forward(self, x):
        if self.input_bn is not None:
            x = self.input_bn(x.transpose(1,2)) # must transpose because batchnorm expects N,C,L rather than N,L,C like multihead
            x = x.transpose(1,2) # revert transpose
        x = self.embed(x)
        return x

class AttnBlock(nn.Module):

    def __init__(self,
                 embed_dim = 4,
                 num_heads = 1,
                 attn_dropout = 0,
                 add_bias_kv = False,
                 kdim = None,
                 vdim = None,
                 ffwd_dims = [16],
                 ffwd_dropout = 0,
                 add_residual = True):
        super().__init__()

        self.attn = nn.MultiheadAttention(embed_dim, num_heads, dropout=attn_dropout, add_bias_kv=add_bias_kv, batch_first=True, kdim=kdim, vdim=vdim)
        self.post_attn_norm = nn.LayerNorm(embed_dim)
        
        # feed-forward
        self.ffwd_dims = ffwd_dims
        module_list = []
        input_dim = embed_dim
        for dim in ffwd_dims:
            module_list.extend([
                nn.Linear(input_dim, dim),
                nn.LayerNorm(dim),
                nn.ReLU(),
                nn.Dropout(ffwd_dropout),
            ])
            input_dim = dim
        self.ffwd = nn.Sequential(*module_list)
        self.add_residual = ffwd_dims[-1] == embed_dim

    def forward(self, query, key, value, key_padding_mask=None, attn_mask=None, flip_query_value=False):

        residual = value

        if flip_query_value:
            query, value = value, query
        
        # perform attention
        value, _ = self.attn(query, key, value, key_padding_mask=key_padding_mask, attn_mask = attn_mask, need_weights=False)
        value += residual
        value = self.post_attn_norm(value)

        # decide if residual should be added
        residual = value # if self.ffwd_dims[-1] == value.shape[-1] else None
        value = self.ffwd(value)
        # if self.add_residual:
        #     value += residual
        # print("here")

        return value

class Step(nn.Module):

    def __init__(
            self, 
            input_dim = 4,
            embed_dims = [4,8,16],
            attn_cfgs = [],
            remove_last_particle=False):
        super().__init__()

        # create model
        self.embed = Embed(input_dim=input_dim, dims=embed_dims)
        self.attn = nn.ModuleList([AttnBlock(**cfg) for cfg in attn_cfgs])
        self.remove_last_particle = remove_last_particle

    def forward(self, x, mask):

        original = x
        # print(original.shape)

        x = self.embed(x)
        # print(x.shape)

        for i in self.attn:
            x = i(x,x,x)
            # print(x.shape)

        # x = x.permute(1,0,2)
        x = torch.softmax(x, dim=-1)
        # print(x.shape)

        # apply mask to fractions
        if mask is not None:
            x = x * mask.unsqueeze(-1).bool()
            # print(x.shape)

        # sum up jets
        x = torch.matmul(x.transpose(2,1), original) # sum up
        # print(x.shape)

        # decide if you want to remove last particle
        if self.remove_last_particle:
            x = x[:,:-1]
        # print(x.shape)

        return x

class StepNetwork(pl.LightningModule):
    def __init__(self, step_cfgs, y_shapes, lr=1e-3):
        super().__init__()
        self.steps = nn.ModuleList([Step(**i) for i in step_cfgs])
        self.save_hyperparameters()
        self.lr = lr
        self.y_shapes = y_shapes

    def forward(self,x,mask):
        for step in self.steps:
            x = step(x, mask)
            mask=None
        return x

    def train_valid_step(self, batch, batch_idx):

        x, y = batch

        # split the y's
        ys = [y[:,i:j] for (i,j) in self.y_shapes]

        loss = 0
        for iStep, (step, yi) in enumerate(zip(self.steps,ys)):
            mask = (x[:,:,0]!=0)
            x = step(x, mask)
            loss += self.loss(x,yi)

        return loss

    def training_step(self, batch, batch_idx):
        loss = self.train_valid_step(batch, batch_idx)
        cur_lr = self.trainer.optimizers[0].param_groups[0]['lr']
        self.log("lr", cur_lr, prog_bar=True, on_step=True)
        self.log("train_loss", loss)
        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.train_valid_step(batch, batch_idx)
        self.log("val_loss", loss)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.parameters(), lr=self.lr)
        return optimizer

    def loss(self, inp, target):
        ''' target is (e,px,py,pz) and inp is (e,px,py,pz) then do L1 norm '''
        loss = torch.nn.functional.l1_loss(inp, target, reduction='mean')
        return loss

def test():

    # create input
    nEvents, nJets, EmbedDim = 3, 7, 4
    x = torch.ones((nEvents, nJets, EmbedDim))
    mask = torch.ones((nEvents, nJets))
    
    # model step 1
    step0 = {
        "input_dim" : 4, 
        "embed_dims" : [32,32,32], 
        "attn_cfgs" :
            [
                {"embed_dim" : 32, "ffwd_dims" : [32,32]},
                {"embed_dim" : 32, "ffwd_dims" : [32,8]},
                {"embed_dim" : 8, "ffwd_dims" : [8,8]},
                {"embed_dim" : 8, "ffwd_dims" : [8,7]},
                {"embed_dim" : 7, "ffwd_dims" : [7,7]},
                {"embed_dim" : 7, "ffwd_dims" : [7,7]},
            ],
        "remove_last_particle" : True
    }
    # model step 2
    step1 = {
        "input_dim" : 4, 
        "embed_dims" : [32,32,32], 
        "attn_cfgs" :
            [
                {"embed_dim" : 32, "ffwd_dims" : [32,32]},
                {"embed_dim" : 32, "ffwd_dims" : [32,8]},
                {"embed_dim" : 8, "ffwd_dims" : [8,8]},
                {"embed_dim" : 8, "ffwd_dims" : [8,2]},
                {"embed_dim" : 2, "ffwd_dims" : [2,2]},
                {"embed_dim" : 2, "ffwd_dims" : [2,2]},
            ],
        "remove_last_particle" : False
    }

    model = StepNetwork([step0,step1])
    y = model(x, mask)

def PtEtaPhiM_to_EPxPyPz(x):
    ''' convert (N,4,P) where 4=(Pt,Eta,Phi,M) to (E,Px,Py,Pz) '''
    pt, eta, phi, m = x.split((1, 1, 1, 1), dim=-1) 
    px = pt * torch.cos(phi)
    py = pt * torch.sin(phi)
    pz = pt * torch.sinh(eta)
    e = torch.sqrt(m**2 + px**2 + py**2 + pz**2)
    x = torch.concat([e,px,py,pz],-1)
    return x

def PtEtaPhiE_to_EPxPyPz(x):
    ''' convert (N,4,P) where 4=(Pt,Eta,Phi,E) to (E,Px,Py,Pz) '''
    pt, eta, phi, e = x.split((1, 1, 1, 1), dim=-1) 
    px = pt * torch.cos(phi)
    py = pt * torch.sin(phi)
    pz = pt * torch.sinh(eta)
    x = torch.concat([e,px,py,pz],-1)
    return x

if __name__ == "__main__":

    # user options
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--inFile", help="Input training file.", default=None, required=True)
    parser.add_argument("-s", "--switch", help="Switch between signal models (GG_rpv, GG_rpv_viaN1).", default="GG_rpv")
    parser.add_argument("-o", "--outDir", help="File name of the output directory", default="./")
    parser.add_argument("-e", "--epochs", help="Number of epochs to train on", default=1, type=int)
    parser.add_argument("-b", "--batch_size", help="Batch size", default=2, type=int)
    parser.add_argument("-d", "--device", help="Device to use.", default=None)
    parser.add_argument("-lr", "--learning_rate", help="Learning rate", default=1e-3, type=float)
    parser.add_argument("-w", "--weights", help="Pretrained weights to use as starting point", default=None)
    ops = parser.parse_args()

    # decide on device
    device = ops.device 
    if not device:
        device = "gpu" if torch.cuda.is_available() else "cpu"

    # model
    step0 = {
        "input_dim" : 4, 
        "embed_dims" : [32,32,32], 
        "attn_cfgs" :
            [
                {"embed_dim" : 32, "ffwd_dims" : [32,32]},
                {"embed_dim" : 32, "ffwd_dims" : [32,8]},
                {"embed_dim" : 8, "ffwd_dims" : [8,8]},
                {"embed_dim" : 8, "ffwd_dims" : [8,3]},
                {"embed_dim" : 3, "ffwd_dims" : [3,3]},
                {"embed_dim" : 3, "ffwd_dims" : [3,3]},
            ],
        "remove_last_particle" : True,
    }
    y_shapes = [(0,2)]
    
    #~~~ Load Data ~~~#
    with h5py.File(ops.inFile, "r") as hf:
        x = np.stack([np.array(hf[f'source/{key}']) for key in ["pt","eta","phi","mass"]],-1) # [N, C=4mom, P] desired input shape
        # get targets as (pt,eta,phi,e)
        y1 = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],-1) # TO-DO: update this to be mass also
        y = [y1]
        # save original shapes and concat
        y = np.concatenate(y,-1)
    
    # convert to tensor
    x, y = torch.Tensor(x), torch.Tensor(y)
    print(x.shape, y.shape)
    x = PtEtaPhiM_to_EPxPyPz(x)[:100]
    y = PtEtaPhiE_to_EPxPyPz(y)[:100]
    # remove events with nan's even though they should not exist TO-DO: understand why these exist in CreateH5files
    mask = x.sum(1).sum(1)
    mask = mask==mask
    x = x[mask]
    y = y[mask]

    # split train test
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, shuffle=True)
    print(f"x_train {x_train.shape}, y_train {y_train.shape}, x_test {x_test.shape}, y_test {y_test.shape}")
    train_dataloader = DataLoader(TensorDataset(x_train, y_train), batch_size=ops.batch_size, shuffle=True, num_workers=0)
    val_dataloader = DataLoader(TensorDataset(x_test, y_test), batch_size=ops.batch_size, shuffle=False, num_workers=0)

    # cleanup
    del x, y, x_train, x_test, y_train, y_test
    gc.collect()

    # create network
    model = StepNetwork([step0], y_shapes=y_shapes)
    if ops.weights:
        ckpt = torch.load(ops.weights,map_location="cpu")
        model.load_state_dict(ckpt["state_dict"])

    # make checkpoint dir
    checkpoint_dir = os.path.join(ops.outDir, f'checkpoints/training_{datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S")}')
    if not os.path.isdir(checkpoint_dir):
        os.makedirs(checkpoint_dir)

    # callbacks
    callbacks = [
        ModelCheckpoint(monitor="val_loss", dirpath=checkpoint_dir, filename='cp-{epoch:04d}', save_top_k=5), # 0=no models, -1=all models, N=n models
        LearningRateMonitor(logging_interval='epoch')
    ]

    # torch lightning trainer
    trainer = pl.Trainer(
        detect_anomaly=True,
        accelerator=device,
        devices=1,
        max_epochs=ops.epochs,
        log_every_n_steps=5,
        callbacks=callbacks,
        precision=32,
        auto_lr_find=True,
        default_root_dir=checkpoint_dir
    )
    
    # tune for learning rate then fit
    trainer.fit(model, train_dataloader, val_dataloader)
    
    # save model
    trainer.save_checkpoint(os.path.join(checkpoint_dir,"finalWeights.ckpt"))
