import torch
import torch.nn as nn

class Set2Set(nn.Module):
    def __init__(self, input_dim, act_fn=nn.ReLU, n_layers=1, n_iters=1):
        '''
        Copied from https://github.com/JiaxuanYou/graph-pooling/blob/master/set2set.py
        Args:
            input_dim: input dim of Set2Set. 
            hidden_dim: the dim of set representation, which is also the INPUT dimension of 
                the LSTM in Set2Set. 
                This is a concatenation of weighted sum of embedding (dim input_dim), and the LSTM
                hidden/output (dim: self.lstm_output_dim).
        '''
        super(Set2Set, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = 2*input_dim
        self.n_layers = n_layers
        self.n_iters = n_iters

        # lstm
        self.lstm = nn.LSTM(self.hidden_dim, self.input_dim, num_layers=self.n_layers, batch_first=True)

        # # convert back to dim of input_dim
        self.pred = nn.Linear(self.hidden_dim, self.hidden_dim) #self.input_dim)
        self.act = act_fn()
        
    def forward(self, embedding):
        '''
        Args:
            embedding: [batch_size x n x d] embedding matrix
        Returns:
            aggregated: [batch_size x d] vector representation of all embeddings
        '''
        # print(embedding.shape)

        batch_size = embedding.size()[0]
        n = embedding.size()[1]

        hidden = (torch.zeros(self.n_layers, batch_size, self.input_dim).to(embedding.device),
                  torch.zeros(self.n_layers, batch_size, self.input_dim).to(embedding.device))

        q_star = torch.zeros(batch_size, 1, self.hidden_dim).to(embedding.device)
        for i in range(self.n_iters):
            # q: batch_size x 1 x input_dim
            q, hidden = self.lstm(q_star, hidden)
            # e: batch_size x n x 1
            e = embedding @ torch.transpose(q, 1, 2)
            a = nn.Softmax(dim=1)(e)
            r = torch.sum(a * embedding, dim=1, keepdim=True)
            q_star = torch.cat((q, r), dim=2)
            # print(q_star[0])
        q_star = torch.squeeze(q_star, dim=1)
        out = self.act(self.pred(q_star))
        
        return out

if __name__ == "__main__":
    nEvents, nJets, inDim = 5, 20, 16
    x = torch.randn((nEvents, nJets, inDim))

    nTargets = 7
    m = nn.ModuleList([Set2Set(input_dim = inDim, n_layers = 3, n_iters = 6)]*nTargets)

    y = []
    for i in m:
        y.append(i(x))
    y = torch.stack(y,1)
    print(y[0])
