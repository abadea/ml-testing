# standard
import h5py
from sklearn.model_selection import train_test_split
import numpy as np
import argparse
import os
import gc
import datetime

# custom
# from ParticleTransformer import PtEtaPhiE_to_EPxPyPz, EPxPyPz_to_PtEtaPhiM
from Transformer import StepLightning

# pytorch and lightning
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, TensorDataset
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelSummary, EarlyStopping, ModelCheckpoint, LearningRateMonitor

def ABPhiC_to_ABCosSinPhiC(x):
    a,b,phi,c = x.split((1, 1, 1, 1), dim=1) 
    return torch.concat([a,b,torch.cos(phi),torch.sin(phi),c],1)

def PtEtaPhiE_to_PtEtaPhiM(x):
    pt,eta,phi,e = x.split((1, 1, 1, 1), dim=1)
    m = torch.sqrt(e**2 - (pt*torch.cos(phi))**2 - (pt*torch.sin(phi))**2 - (pt*torch.sinh(eta))**2)
    return torch.concat([pt,eta,phi,m],1)

def main():

    # user options
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--inFile", help="Input training file.", default=None, required=True)
    parser.add_argument("-s", "--switch", help="Switch between signal models (GG_rpv, GG_rpv_viaN1).", default="GG_rpv")
    parser.add_argument("-o", "--outDir", help="File name of the output directory", default="./")
    parser.add_argument("-e", "--epochs", help="Number of epochs to train on", default=1, type=int)
    parser.add_argument("-b", "--batch_size", help="Batch size", default=2, type=int)
    parser.add_argument("-d", "--device", help="Device to use.", default=None)
    parser.add_argument("-lr", "--learning_rate", help="Learning rate", default=1e-3, type=float)
    parser.add_argument("-w", "--weights", help="Pretrained weights to use as starting point", default=None)
    parser.add_argument("-p", "--predict", help="Just do prediction", action="store_true")
    ops = parser.parse_args()

    # decide on device
    device = ops.device 
    if not device:
        device = "gpu" if torch.cuda.is_available() else "cpu"

    encoder_config = {
        "input_dim" : 5,
        "embed_input" : True,
        "embed_dims" : [16]
    }
    num_encoder_blocks = 2
    encoder_config["attn_block_cfgs"] = [{"embed_dim" : encoder_config["embed_dims"][-1], "ffwd_on" : True}] * num_encoder_blocks

    num_decoder_blocks = 2
    decoder_config = {
        "nTargetJets" : 2,
        "cross_attn_block_cfgs" : [{"embed_dim" : encoder_config["embed_dims"][-1], "ffwd_on": True}] * num_decoder_blocks,
        "self_attn_block_cfgs" : [{"embed_dim" : encoder_config["embed_dims"][-1], "ffwd_on": True}] * num_decoder_blocks,
        "ffwd_on" : False,
        "ffwd_dim" : 4,
        "num_target_objs" : 2
    }

    model = StepLightning(encoder_config=encoder_config, decoder_config=decoder_config, lr=ops.learning_rate, init_weights=True)

    #model = model.to(device)
    #print(device)

    if ops.weights:
        ckpt = torch.load(ops.weights,map_location="cpu")
        model.load_state_dict(ckpt["state_dict"])
    
    #~~~ Load Data ~~~#
    with h5py.File(ops.inFile, "r") as hf:
        x = np.stack([np.array(hf[f'source/{key}']) for key in ["pt","eta","phi","mass"]],1) # [N, C=4mom, P] desired input shape
        
        # get targets as (pt,eta,phi,e)
        if ops.switch == "GG_rpv":
            y1 = np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["pt","eta","phi","e"]],1) # TO-DO: update this to be mass also
            y = [y1]
            
        # save original shapes and concat
        y_shapes = [i.shape[-1] for i in y]
        y = np.concatenate(y,-1)
    
    # convert to tensor
    x, y = torch.Tensor(x), torch.Tensor(y)
    x = ABPhiC_to_ABCosSinPhiC(x)
    y = PtEtaPhiE_to_PtEtaPhiM(y)

    # remove events with nan's even though they should not exist TO-DO: understand why these exist in CreateH5files
    mask = x.sum(1).sum(1)
    mask = mask==mask
    x = x[mask]
    y = y[mask]
    print(f"Number of events with nan: {(~mask).sum()}")
    print(np.where(x!=x))
    
    # flip axis 
    x = x.transpose(2,1)
    y = y.transpose(2,1)

    # predict
    if ops.predict:
        # quick prediction
        model.eval()
        with torch.no_grad():
            yh = model(x).detach().numpy()
        # save to file
        print(y.shape, yh.shape)
        y = y.numpy().transpose(0,2,1)
        yh = yh.transpose(0,2,1)
        outData = {"y":y,"y_hat":yh}
        with h5py.File(os.path.join(ops.outDir,"testTransformer.h5"),"w") as hf:
            for key, val in outData.items():
                hf.create_dataset(key,data=val)
        return

    # split data
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, shuffle=True)
    print(f"x_train {x_train.shape}, y_train {y_train.shape}, x_test {x_test.shape}, y_test {y_test.shape}")
    train_dataloader = DataLoader(TensorDataset(x_train, y_train), batch_size=ops.batch_size, shuffle=True, num_workers=0)
    val_dataloader = DataLoader(TensorDataset(x_test, y_test), batch_size=ops.batch_size, shuffle=False, num_workers=0)

    # cleanup
    del x_train, x_test, y_train, y_test # x, y, 
    gc.collect()

    # make checkpoint dir
    checkpoint_dir = os.path.join(ops.outDir, f'checkpoints/training_{datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S")}')
    if not os.path.isdir(checkpoint_dir):
        os.makedirs(checkpoint_dir)

    # callbacks
    callbacks = [
        # ModelSummary(max_depth=-1),
        # EarlyStopping(monitor="val_loss", mode="min", min_delta=0.0, patience=10, verbose=True),
        ModelCheckpoint(monitor="val_loss", dirpath=checkpoint_dir, filename='cp-{epoch:04d}', save_top_k=1), # 0=no models, -1=all models, N=n models, set save_top_k=-1 to save all checkpoints
        LearningRateMonitor(logging_interval='epoch')
    ]

    # torch lightning trainer
    trainer = pl.Trainer(
        overfit_batches=0.1,
        #detect_anomaly=True,
        # gradient_clip_val=0.5,
        # track_grad_norm=2,
        accelerator=device,
        devices=1,
        max_epochs=ops.epochs,
        log_every_n_steps=5,
        callbacks=callbacks,
        precision=32,
        auto_lr_find=True,
        default_root_dir=checkpoint_dir
        #auto_scale_batch_size="binsearch"
    )
    
    # fit
    trainer.fit(model, train_dataloader, val_dataloader)
    
    # save model
    trainer.save_checkpoint(os.path.join(checkpoint_dir,"finalWeights.ckpt"))


if __name__ == "__main__":
    main()
