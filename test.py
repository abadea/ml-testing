import itertools
import torch
import h5py
import numpy as np

def DeltaPhi(phi1,phi2):
    ''' delta phi = phi_1 - phi_2 needs to be handled carefully. copying the implementation from root into numpy vectorization 
    https://root.cern.ch/doc/v606/GenVector_2VectorUtil_8h_source.html#l00061 '''
    dphi = phi1 - phi2
    dphi[dphi > torch.pi] -= 2.0*torch.pi
    dphi[dphi <= -torch.pi] += 2.0*torch.pi
    return dphi

def DeltaEta(eta1,eta2):
	deta = eta1 - eta2
	return deta

def DeltaR(eta1, eta2, phi1, phi2):
	dphi = DeltaPhi(phi1,phi2)
	deta = DeltaEta(eta1,eta2)
	dr = torch.sqrt(deta**2 + dphi**2)
	return dr

y_shapes = [7,2]
y_perms = [torch.Tensor(list(itertools.permutations(range(N)))).long() for N in y_shapes]

iStep = 1
i = y_perms[iStep]

Nb = 3
Nf = 5 # pt,eta,cos(phi),sin(phi),m
Nc = 7
inp = torch.randn((Nb,Nf,Nc))
# target = torch.ones((Nb,Nf,Nc))

# extract etaphi
inp_etaphi = inp[:,[1,2]] # fix because inp is eta,cos(phi),sin(phi)
inp_etaphi[:,1] = torch.arccos(inp_etaphi[:,1])
# target_etaphi = target[:,[1,2]]
# target_etaphi[:,1] = torch.arccos(target_etaphi[:,1])


hf = h5py.File("../inputs/user.abadea.504545.e8258_e7400_s3126_r10724_r10726_p5083.29777524._000001.trees_SRRPV_minJetPt50_minNjets0_maxNjets20_v0.h5","r")
target_etaphi = torch.Tensor(np.stack([np.array(hf[f'truth/truth_parent_{key}']) for key in ["eta","phi"]],1)[:Nb])
print(inp_etaphi.shape, target_etaphi.shape)
	
# make permutations
inp_etaphi = inp_etaphi[:,:,i]
target_etaphi = target_etaphi.unsqueeze(2).repeat(1,1,i.shape[0],1)
print(inp_etaphi.shape, target_etaphi.shape)

# compute dR, average, get argmin
dR = DeltaR(inp_etaphi[:,0], target_etaphi[:,0], inp_etaphi[:,1], target_etaphi[:,1]) # need to update this
print(dR.shape)
dR = dR.mean(-1)
print(dR[:10])
dR = torch.argmin(dR,-1)
print(dR.shape)

# get permutation from argmin
i = i[dR]
print(i[:10])
# print(inp)
# use permutation to permute input
inp = torch.gather(inp,-1,i.unsqueeze(1).repeat(1,inp.shape[1],1).long())
# inp = inp[:,:,i] # need to update this
print(inp.shape)